﻿using BuisnessObject.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL.Context
{
    class DBContext : DbContext
    {

        public DBContext() { }

        public DBContext(DbContextOptions<DBContext> options) : base(options) { }

        public DbSet<user> Users { get; set; }
        public DbSet<roles> Roles { get; set; }
        public DbSet<product> products { get; set; }
        public DbSet<order> orders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"data source=INW-921\SQLEXPRESS;initial catalog=practicedb;integrated security=true;TrustServerCertificate=true");
        }


    }
}
