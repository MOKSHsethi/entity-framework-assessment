﻿using BuisnessObject.Models;
using DAL;
using System.Threading.Tasks.Dataflow;

namespace BAL
{
    public class Bal
    {
        dal dal = new dal(); 

            public int AddUser(user user)
        {
            dal.AddUser(user);
            return 0;
        }

        public int AddProduct(product product)
        {
            dal.AddProduct(product);
            return 1;
        }
        public int DelProduct(int id)
        {
            dal.DelProduct(id);
            return 1;
        }
        public int UpdateProduct(int id , product product)
        {
            dal.UpdateProduct(id  , product);
            return 0;
        }

      
        public int DeleteUser(int id)
        {
            dal.DeleteUser(id);
            return 1;
        }

        public int PlaceOrder(order order)
        {
            return dal.PlaceOrder(order);
        }
    }
}